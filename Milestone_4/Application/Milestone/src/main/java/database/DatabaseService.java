package database;

import beans.Blog;
import beans.User;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 *
 * Implementation for the Database Interface
 * Heavily relies on UserDAO and BlogDAO implementations
 */
@Stateless
@Local(DatabaseInterface.class)
public class DatabaseService implements DatabaseInterface{
    
    private UserDAO userDAO = new UserDAO();
    private BlogDAO blogDAO = new BlogDAO();
    
    public DatabaseService() {
        
    }
    
    public List<User> readAllUsers() {
        return userDAO.readAllUsers();
    }
    
    public int insertOne(User user) {
        return userDAO.insertOne(user);
    }
    
    public User selectUserById(int id) {
        return userDAO.selectUserById(id);
    }
    
    public User selectUserByUsrName(String usrName) {
        return userDAO.selectUserByUsrName(usrName);
    }
    
    public int deleteUserById(int id) {
        return userDAO.deleteUserById(id);
    }
    
    public int updateUser(User user) {
        return userDAO.updateUser(user);
    }
    

    //
    // All exposed methods for the Blog DAO
    //
    
    public List<Blog> readAllBlogs() {
        return blogDAO.readAllBlogs();
    }
    
    public int insertOne(Blog blog) {
        return blogDAO.insertOne(blog);
    }
    
    public Blog selectBlogById(int id) {
        return blogDAO.selectBlogById(id);
    }
    
    public int deleteBlogById(int id) {
        return blogDAO.deleteBlogById(id);
    }
    
    public int updateBlog(Blog blog) {
        return blogDAO.updateBlog(blog);
    }
    
}
