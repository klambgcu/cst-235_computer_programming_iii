package beans;

import java.io.FileWriter;
import java.io.IOException;
import javax.faces.bean.ManagedBean;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
public class User {

    private int user_id;
    private String firstName;
    private String lastName;
    private String usrName;
    private String password;
    private String experience;

    // Constructors
    public User() {
        this.user_id = 0;
        this.firstName = null;
        this.lastName = null;
        this.usrName = null;
        this.password = null;
        this.experience = null;
    }

    public User(int user_id, String firstName, String lastName, String usrName, String password, String experience) {
        super();
        this.user_id = user_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.usrName = usrName;
        this.password = password;
        this.experience = experience;
    }

    // Methods
    public void onSubmit() {

        try {
            FileWriter myWriter = new FileWriter("filename.txt", true);
            myWriter.write(toString());
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        return "User [user_id=" + user_id + ", firstName=" + firstName + ", lastName=" + lastName + ", usrName=" + usrName + ", password="
                + password + ", experience=" + experience + "]";
    }

    public String toStringTabDelimited() {
        return firstName + "\t" + lastName + "\t" + usrName + "\t" + password + "\t" + experience + System.lineSeparator();
    }
  
    // Getters & Setters

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

}
