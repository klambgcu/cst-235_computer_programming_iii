package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import java.util.List;
import beans.Blog;
import beans.Blogs;
import database.DatabaseService;
import java.util.ArrayList;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */


@Stateless
@Local(BlogBusinessInterface.class)
@Alternative
public class BlogsBusinessService implements BlogBusinessInterface {

    
    private final DatabaseService db = new DatabaseService();

    @Override
    public List<Blog> getBlogs()
    {
        return readAll();
    }
    
    @Override
    public int deleteOne(int id) {
        
        return db.deleteBlogById(id);
    }

    @Override
    public int insertOne(Blog blog) {
        
        return db.insertOne(blog);
    }

    @Override
    public List<Blog> readAll() {
        
        return db.readAllBlogs();
    }

    @Override
    public int updateOne(Blog blog) {
        
        return db.updateBlog(blog);
    }

}
