package controllers;

import beans.Blog;
import beans.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import business.BlogBusinessInterface;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle Blogging actions to the appropriate page
 *
 */
@ManagedBean
@Named
@SessionScoped
public class BlogController {
    
    @Inject
    BlogBusinessInterface service;
            
    // Used to push to DB
    public String onSubmit(Blog blog) 
    {
        System.out.println("We Will use this to push to DB\n\n");

        // FacesContext context = FacesContext.getCurrentInstance();
        // Blog blog = context.getApplication().evaluateExpressionGet(context, "#{blog}", Blog.class);

        //
        // Add blog to list and write to file
        //
        
        User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        blog.setAuth_id(user.getUser_id());
        service.insertOne(blog);
                
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("blog", blog);
        
        return "listing_blog.xhtml";
    }
 
    public String onClickEdit(Blog blog) {

        System.out.println("BlogController onClickEdit() Executing.");
        System.out.println("Blog: " + blog.toString());

        //FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Blog", blog);
        //FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("blog", blog);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("blog", blog);
        
        return "edit_blog.xhtml?faces-redirect=true";
    }

    public String onSubmitEdit(Blog blog) {
    
        System.out.println("onSubmitEdit: Blog " + blog.toString());
        return "listing_blog.xhtml";
    }
    
    public String onCancelEdit() {
    
        return "listing_blog.xhtml";
    }
    
    public String onClickDelete(Blog blog) {

        System.out.println("BlogController onClickDelete() Executing.");
        System.out.println("Blog: " + blog.toString());

        //FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("blog", blog);
        //FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("blog", blog);
        
        service.deleteOne(blog.getBlog_id());
        
        //return "delete_blog.xhtml";
        return "listing_blog.xhtml?faces-redirect=true";
    }

    public String onSubmitDelete(Blog blog) {
    
        System.out.println("onSubmitEdit: Blog " + blog.toString());
        return "listing_blog.xhtml";
    }
    
    public String onCancelDelete() {
    
        return "listing_blog.xhtml";
    }
       
    
    public BlogBusinessInterface getService() {
        return service;
    }
    
 }
