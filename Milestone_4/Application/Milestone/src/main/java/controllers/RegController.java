package controllers;

import beans.User;
// import beans.Users;
import java.io.FileWriter;
import java.io.IOException;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import business.UserBusinessInterface;
import database.UserDAO;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle User Registration
 *
 */
@ManagedBean
public class RegController {

    @EJB
    UserBusinessInterface service;
    
    public String onSubmit() {

        FacesContext context = FacesContext.getCurrentInstance();
        User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

        //
        // Check if user already registered
        //
        if (service.userAlreadyExists(user.getUsrName()) == false) {
            //
            // Add user to database
            //
            service.insertOne(user);
            user = service.getUserByUsrName(user.getUsrName()); // Load user id after insert
            
            System.out.println("RegController: User = " + user.toString());
            

            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
            return "index.xhtml";
        } else {
            
            // Add View Faces Message
            String errorMessage = "User Account Already Exists.";
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, errorMessage);

            // Add the message into context for a specific component
            FacesContext.getCurrentInstance().addMessage("reg-form:message", message);
            
            return "registration.xhtml";
        }

    }
}
