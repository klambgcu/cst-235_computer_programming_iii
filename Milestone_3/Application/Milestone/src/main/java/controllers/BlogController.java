package controllers;

import beans.Blog;
import java.io.FileWriter;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import services.BlogBusinessInterface;
/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle Blogging actions to the appropriate page
 *
 */
@ManagedBean
@Named
@SessionScoped
public class BlogController {
    
    @Inject
    BlogBusinessInterface service;
    
    // Used to push to DB
    public String onSubmit(Blog blog) 
    {
        System.out.println("We Will use this to push to DB\n\n");

        // FacesContext context = FacesContext.getCurrentInstance();
        // Blog blog = context.getApplication().evaluateExpressionGet(context, "#{blog}", Blog.class);

        //
        // Add blog to list and write to file
        //    
        service.getBlogs().add(blog);
        
        try
        {
          FileWriter myWriter = new FileWriter("blogs.txt", true);
          myWriter.write(blog.toStringTabDelimited());
          myWriter.close();
          System.out.println("Successfully wrote to the file.");
        }
        catch (IOException e)
        {
          System.out.println("An error occurred.");
          e.printStackTrace();
            
        }

        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("blog", blog);
        
        return "index.xhtml";
    }
 
    public BlogBusinessInterface getService() {
        return service;
    }
}
