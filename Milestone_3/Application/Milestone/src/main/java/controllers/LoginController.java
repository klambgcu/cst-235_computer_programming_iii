package controllers;

import beans.User;
import beans.Users;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle User Login
 *
 */
@ManagedBean
@Named
@SessionScoped
public class LoginController {

    public String onSubmit(User user) {

        //FacesContext context = FacesContext.getCurrentInstance();
        //User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
        //
        // Check if user is registered
        //
        if (Users.getInstance().isValidUser(user)) {
            // Incorrect information - return to login page
            return "login.xhtml";
        } else {
            // Valid user - proceed to index page
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userName", user.getUsrName());
            return "index.xhtml";
        }
    }
}
