package beans;

import java.util.Date;
import java.util.Objects;
import javax.faces.bean.ManagedBean;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Singleton to obtain list of users from file
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
public final class Blog {

    private final int blog_id; // wont change
    private String title;
    private final String auth_id; //this wont change
    private Date date;
    private String blog;
    private Boolean isEdited;
    private Date edited_date;

    public Blog() {
        this.blog_id = 0;
        this.title = "";
        this.auth_id = "";
        this.date = new Date();
        this.blog = "";
        this.isEdited = false;
        this.edited_date = new Date();
    }

    /**
     * *
     *
     * @param t - Title : S
     * @param usrID - Author's ID : S
     * @param b - Blog Text : S
     * @param e - If edited : B
     */
    public Blog(String t, String usrID, String b, boolean e) {
        this.blog_id = hashCode();
        this.title = t;
        this.auth_id = usrID;
        this.date = new Date();
        this.blog = b;
        this.isEdited = e;
        this.edited_date = new Date();
    }

    public void edit_blog(String b) {
        this.blog = b;
        this.isEdited = true;
        this.edited_date = new Date();
    }

    public String toStringTabDelimited() {
        return this.title + "\t" + this.auth_id + "\t" + this.blog + "\t" + this.isEdited + System.lineSeparator();
    }

    @Override
    public String toString() {
        return "Blog{" + "blog_id=" + blog_id + ", title=" + title + ", auth_id=" + auth_id + ", date=" + date + ", blog=" + blog + ", isEdited=" + isEdited + ", edited_date=" + edited_date + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.blog_id);
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.auth_id);
        hash = 97 * hash + Objects.hashCode(this.date);
        hash = 97 * hash + Objects.hashCode(this.blog);
        hash = 97 * hash + Objects.hashCode(this.isEdited);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = true;
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            isEqual = false;
        } else if (getClass() != obj.getClass()) {
            isEqual = false;
        }
        final Blog other = (Blog) obj;
        if (!Objects.equals(this.blog_id, other.blog_id)) {
            isEqual = false;
        }
        if (!Objects.equals(this.title, other.title)) {
            isEqual = false;
        }
        if (!Objects.equals(this.auth_id, other.auth_id)) {
            isEqual = false;
        }
        if (!Objects.equals(this.blog, other.blog)) {
            isEqual = false;
        }
        return isEqual;
    }

    public int getBlog_id() {
        return blog_id;
    }

//    public void setBlog_id(int blog_id) {
//        this.blog_id = blog_id;
//    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuth_id() {
        return auth_id;
    }

//    public void setAuth_id(String auth_id) {
//        this.auth_id = auth_id;
//    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public Boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public Date getEdited_date() {
        return date;
    }

    public void setEdited_date(Date date) {
        this.date = date;
    }

    
}
