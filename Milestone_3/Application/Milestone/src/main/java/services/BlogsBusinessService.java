package services;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import java.util.List;
import beans.Blog;
import beans.Blogs;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */


@Stateless
@Local(BlogBusinessInterface.class)
@Alternative
public class BlogsBusinessService implements BlogBusinessInterface {

    @Override
    public List<Blog> getBlogs() {
        return Blogs.getInstance().getBlogList();
    }

    @Override
    public void setOrders(List<Blog> blogs) {
        Blogs.getInstance().setBlogList(blogs);
    }
    
}
