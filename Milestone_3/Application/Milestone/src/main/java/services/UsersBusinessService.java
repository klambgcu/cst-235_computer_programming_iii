package services;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import java.util.List;
import beans.User;
import beans.Users;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */


@Stateless
@Local(UserBusinessInterface.class)
@Alternative
public class UsersBusinessService implements UserBusinessInterface {

    @Override
    public List<User> getUsers() {
        return Users.getInstance().getUserList();
    }

    @Override
    public void setUsers(List<User> users) {
        Users.getInstance().setUserList(users);
    }

    @Override
    public Boolean userAlreadyExists(String usrName) {
        return Users.getInstance().userAlreadyExists(usrName);
    }
    
    @Override
    public Boolean isValidUser(User user) {
        return Users.getInstance().isValidUser(user);
    }
}
