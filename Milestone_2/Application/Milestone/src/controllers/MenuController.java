/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

package controllers;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

@ManagedBean
@Named
@SessionScoped
/**
 * Controller to handle Menu bar selections
 * Delegates actions to the appropriate page
 *
 */
public class MenuController {
		
	public MenuController() {
		
	}
	
	public String handleLogin() {

		System.out.println("MenuController handleLogin() Executing.");
		
		return "login.xhtml";
	}
	
	public String handleLogout() {

		System.out.println("MenuController handleLogout() Executing.");
		
		// Invalidate session (i.e. user not valid anymore)
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("userName");
	
		// Return back to main
		return "index.xhtml";		
	}
	
	public String handleRegistration() {

		System.out.println("MenuController handleRegistration() Executing.");
		
		return "registration.xhtml";		
	}
	
	public String handleCreate() {

		System.out.println("MenuController handleCreate() Executing.");
		
		// TODO - define appropriate page, return string name of page
		return "index.xhtml";		
	}
	
	public String handleDelete() {

		System.out.println("MenuController handleDelete() Executing.");
		
		// TODO - define appropriate page, return string name of page
		return "index.xhtml";		
	}
	
	public String handleEdit() {

		System.out.println("MenuController handleEdit() Executing.");
		
		// TODO - define appropriate page, return string name of page
		return "index.xhtml";		
	}
		
	public String handleReport1() {

		System.out.println("MenuController handleReport1() Executing.");
		
		// TODO - define appropriate page, return string name of page
		return "index.xhtml";		
	}
	
	public String handleAbout() {

		System.out.println("MenuController handleAbout() Executing.");
		
		// TODO - define appropriate page, return string name of page
		return "index.xhtml";		
	}
	
	public Boolean menuDisabled(String source) {
		
		System.out.println("MenuController menuDisabled() Executing.");

		// TODO - Determine how we know user has login successfully.
		// Assume we will store user name, userID in Context
		String userName = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userName");
		Boolean result = (userName == null);
		
		// Need opposite logic - user logged in, true => false
		if ( ("Login".equals(source)) || ("Register".equals(source)) )
			result = !result;
		
		return result;		
		
	}
		
}
