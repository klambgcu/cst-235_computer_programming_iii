/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

package controllers;

import java.io.FileWriter;
import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import beans.User;
import beans.Users;

@ManagedBean
public class RegController {

	public String onSubmit() {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		//
		// Check if user already registered
		//
		if (Users.getInstance().userAlreadyExists(user.getUsrName()) == false)
		{
			//
			// Add user to list and write to file
			//
			Users.getInstance().getUserList().add(user);
			try
			{
		      FileWriter myWriter = new FileWriter("filename.txt", true);
		      myWriter.write(user.toStringTabDelimited());
		      myWriter.close();
		      System.out.println("Successfully wrote to the file.");
		    }
			catch (IOException e)
			{
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		}

		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);

		return "index.xhtml";

	}
}
