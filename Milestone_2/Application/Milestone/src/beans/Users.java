/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Singleton to obtain list of users from file
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

package beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Users {

	private static Users single_instance = null;
	
	private List<User> userList = new ArrayList<User>();
	
	private Users()
	{
		if (userList.size() == 0)
		{
			File file = new File("filename.txt");
			
			if (file.exists())
			{
				try
				{
					Scanner s = new Scanner(file);
					
					while (s.hasNextLine())
		            {
						String line = s.nextLine();
						String[] items = line.split("\t");
						
						if (items.length >= 5)
						{
							User user = new User(items[0], items[1], items[2], items[3], items[4]);
							userList.add(user);
						}
						else
						{
							System.out.println("USERS: " + line);
						}
		            }
					s.close();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public static Users getInstance() 
	{
		if (single_instance == null) 
            single_instance = new Users(); 
  
        return single_instance; 
	}
	
	public Boolean userAlreadyExists(String usrName)
	{
		Boolean result = false;
		
		for (User u : userList)
		{
			if (u.getUsrName().equals(usrName))
			{
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	public Boolean isValidUser(User user)
	{
		Boolean result = false;
		
		for (User u : userList)
		{
			if (u.getUsrName().equals(user.getUsrName()) && u.getPassword().equals(user.getPassword()))
			{
				result = true;
				break;
			}
		}
		
		return result;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

}
