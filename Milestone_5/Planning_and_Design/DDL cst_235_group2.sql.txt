--
-- MySQL DDL creation script Group2 CST-235 Milestone
--
-- Note:
-- 
-- Download/install MySQL
-- Tested on version 5.7.17
-- Default login -u root -p root
-- Login for group2 CST 235 assignment
-- mysql -u group2 -p
-- enter group2 at password prompt
--
-- To Start MYSQL Server:
-- ----------------------------------
-- "C:\Program Files\MySQL\MySQL Server 5.7\bin\mysqld.exe" "--defaults-file=C:\ProgramData\MySQL\MySQL Server 5.7\my.ini"

-- To Run SQL:
-- ----------------------------------
-- cd "C:\Program Files\MySQL\MySQL Server 5.7\bin\"
-- mysql -u root -p

-- To Stop MYSQL Server:
-- ----------------------------------
-- cd "C:\Program Files\MySQL\MySQL Server 5.7\bin\"
-- mysqladmin -u root -p shutdown


--
-- Create a new database: cst_235
--
CREATE DATABASE IF NOT EXISTS cst_235 DEFAULT CHARACTER SET utf8;

--
-- Create a new user on the local host
--
CREATE USER 'group2'@'localhost' IDENTIFIED BY 'group2';

--
-- Grant privileges to group2 on cst_235
--
GRANT ALL PRIVILEGES ON cst_235.* TO 'group2'@'localhost';
GRANT USAGE ON *.* TO 'group2'@'localhost';

--
-- Force grants to take affect
--
FLUSH PRIVILEGES;

--
-- Start using the database to add tables
--
USE cst_235;

--
-- Create table structure for user
--
create table user
(
	user_id           int(11)        not null auto_increment,
	firstname         varchar(50)    not null,
	lastname          varchar(50)    not null,
	usrname           varchar(50)    not null,
	password          varchar(50)    not null,
	experience        varchar(50)    not null,
	primary key (user_id),
	unique key usrname_unique (usrname)
) engine=innodb auto_increment=1 default charset=utf8;

--
-- Create table structure for blog
-- Do we want to foreign key auth_id to user.user_id?
-- Does title need to be unique?
--
create table blog
(
	blog_id           int(11)        not null auto_increment,
	title             varchar(50)    not null,
        auth_id           int(11)        not null,
	date              datetime       not null,
        blog              varchar(500)   not null,
        is_edited         bit            not null,
        edited_date       datetime       not null,
	primary key (blog_id)
) engine=innodb auto_increment=1 default charset=utf8;

--
-- Test scripts
--
-- insert into user (firstname, lastname, usrname, password, experience) values ("Kelly", "Lamb", "kl", "kl", "Student");
-- select * from user;
-- 
-- insert into blog (title, auth_id, date, blog, is_edited, edited_date) values ("Blog 1", 1, "2021-03-13", "This is a blog entry.", 0, "2021-03-13");
-- select * from blog;
--
