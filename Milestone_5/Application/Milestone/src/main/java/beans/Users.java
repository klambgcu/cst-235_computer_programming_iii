package beans;

import database.DatabaseService;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.faces.bean.ManagedBean;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Singleton to obtain list of users from file
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */


@ManagedBean
public class Users {

    private static Users single_instance = null;

    private List<User> userList = new ArrayList<User>();
    private DatabaseService db = new DatabaseService();

    private Users() {
        if (userList.isEmpty()) {
            userList = db.readAllUsers();
        }
    }

    public static Users getInstance() {
        if (single_instance == null) {
            single_instance = new Users();
        }

        return single_instance;
    }

    public Boolean userAlreadyExists(String usrName) {
        Boolean result = false;

        for (User u : userList) {
            if (u.getUsrName().equals(usrName)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public Boolean isValidUser(User user) {
        Boolean result = false;

        System.out.println("Users: Validating User: " + user.getUsrName() + " Password: " + user.getPassword());
        System.out.println("Users: LOOP BEGIN=====================");
        
        for (User u : userList) {
            System.out.println("user: " + u.getUsrName() + " pass: " + u.getPassword());
            if (u.getUsrName().equals(user.getUsrName()) && u.getPassword().equals(user.getPassword())) {
                result = true;
                break;
            }
        }

        System.out.println("Users: LOOP END======================= " + result.toString());

        return result;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

}
