package beans;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.faces.bean.ManagedBean;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Singleton to obtain list of users from file
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
public class Blogs {

    private static Blogs single_instance = null;

    private List<Blog> blog_list = new ArrayList<>();

    private Blogs() {
        this.blog_list = new ArrayList<>();
        
        //
        // Netbeans / Glassfish store this file in the follow directory
        // C:\Users\{User Name}\GlassFish_Server\glassfish\domains\domain1\config
        //
        
        /* ****************************************
         * This section not utilized after switch to database
         *
        File file = new File("blogs.txt");

        
        
        try {
            Scanner s = new Scanner(file);

            while (s.hasNextLine()) {
                String line = s.nextLine();
                String[] items = line.split("\t");

                if (items.length >= 4) {
                    int auth_id = Integer.parseInt(items[1]);
                    Blog b = new Blog(items[0], auth_id, items[2], Boolean.getBoolean(items[3]));
                    b.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(items[4]));
                    b.setEdited_date(new SimpleDateFormat("yyyy-MM-dd").parse(items[5]));
                    blog_list.add(b);
                } else {
                    System.out.println("BLOG_LIST: " + line);
                }
            }
            s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        *********************************************/
    }


    public Blog search(String search_key) {
        Blog b = null;

        for (Blog el : blog_list) {
            if (el.toString().contentEquals(search_key)) {
                b = el;
                break;
            }
        }

        return b;
    }

    public static Blogs getInstance() {
        if (single_instance == null) {
            single_instance = new Blogs();
        }

        return single_instance;
    }
    
    public List<Blog> getBlogList() {
        return blog_list;
    }

    public void setBlogList(List<Blog> blog_list) {
        this.blog_list = blog_list;
    }

}
