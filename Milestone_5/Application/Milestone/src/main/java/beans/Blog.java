package beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.faces.bean.ManagedBean;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Singleton to obtain list of users from file
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
public final class Blog {

    private int blog_id;
    private String title;
    private int auth_id;
    private Date date;
    private String blog;
    private Boolean isEdited;
    private Date edited_date;

    /**
     * 
     * Default Constructor
     */
    public Blog() {
        Date currentDate = getDateWithoutTime();
        this.blog_id = 0;
        this.title = "";
        this.auth_id = 0;
        this.date = currentDate;
        this.blog = "";
        this.isEdited = false;
        this.edited_date = currentDate;
    }

    /**
     * Constructor - full parameters
     * @param blog_id
     * @param title
     * @param auth_id
     * @param date
     * @param blog
     * @param isEdited
     * @param edited_date 
     */
    public Blog(int blog_id, String title, int auth_id, Date date, String blog, Boolean isEdited, Date edited_date) {
        this.blog_id = blog_id;
        this.title = title;
        this.auth_id = auth_id;
        this.date = date;
        this.blog = blog;
        this.isEdited = isEdited;
        this.edited_date = edited_date;
    }
    
    /**
     * *
     *
     * @param title - Title : S
     * @param user_id - Author's ID : S
     * @param blog - Blog Text : S
     * @param isEdited - If edited : B
     */
    public Blog(String title, int user_id, String blog, boolean isEdited) {
        this.blog_id = hashCode(); // Override this with id from database
        this.title = title;
        this.auth_id = user_id;
        this.date = getDateWithoutTime();
        this.blog = blog;
        this.isEdited = isEdited;
        this.edited_date = getDateWithoutTime();
    }

    private Date getDateWithoutTime()
    {
        Date dateWithoutTime = new Date();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateWithoutTime = sdf.parse(sdf.format(new Date()));
        } catch (Exception e) {e.printStackTrace();}// Do nothing
        
        return dateWithoutTime;
    }
    
    public void edit_blog(Blog b) {
        this.blog = b.getBlog();
        this.title = b.getTitle();
        this.isEdited = true;
        this.edited_date = getDateWithoutTime();
    }

    public String toStringTabDelimited() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
        return this.title + "\t" + 
               this.auth_id + "\t" +
               this.blog + "\t" +
               this.isEdited + "\t" +
               formatter.format(this.date) + "\t" +
               formatter.format(this.edited_date) + "\t" +
               System.lineSeparator();
    }

    @Override
    public String toString() {
        return "Blog{" + 
                "blog_id=" + blog_id + 
                ", title=" + title +
                ", auth_id=" + auth_id + 
                ", date=" + date + 
                ", blog=" + blog + 
                ", isEdited=" + isEdited + 
                ", edited_date=" + edited_date + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.blog_id);
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.auth_id);
        hash = 97 * hash + Objects.hashCode(this.date);
        hash = 97 * hash + Objects.hashCode(this.blog);
        hash = 97 * hash + Objects.hashCode(this.isEdited);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = true;
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            isEqual = false;
        } else if (getClass() != obj.getClass()) {
            isEqual = false;
        }
        final Blog other = (Blog) obj;
        if (!Objects.equals(this.blog_id, other.blog_id)) {
            isEqual = false;
        }
        if (!Objects.equals(this.title, other.title)) {
            isEqual = false;
        }
        if (!Objects.equals(this.auth_id, other.auth_id)) {
            isEqual = false;
        }
        if (!Objects.equals(this.blog, other.blog)) {
            isEqual = false;
        }
        return isEqual;
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAuth_id() {
        return auth_id;
    }

    public void setAuth_id(int auth_id) {
        this.auth_id = auth_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public Boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    public Date getEdited_date() {
        return edited_date;
    }

    public void setEdited_date(Date date) {
        this.edited_date = date;
    }

    
}
