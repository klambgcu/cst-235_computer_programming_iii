package controllers;

import beans.User;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import business.UserBusinessInterface;
import database.UserDAO;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle User Login
 *
 */
@ManagedBean
@Named
@SessionScoped
public class LoginController {
    
    @EJB
    UserBusinessInterface service;

    public String onSubmit(User user) {

        //
        // Check if user is registered
        //
        System.out.println("LoginController: User: " + user.getUsrName() + "Password: " + user.getPassword());

        if (service.isValidUser(user)) {
            // Valid user - proceed to index page
            
            // Obtain full user information from database - include user_id to store in session
            user = service.getUserByUsrName(user.getUsrName());
            
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userName", user.getUsrName());
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
            return "listing_blog.xhtml";
        } else {
            // Incorrect information - return to login page
            return "login.xhtml";
        }
    }
}
