package controllers;

import beans.Blog;
import beans.Blogs;
import beans.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import business.BlogBusinessInterface;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle Blogging actions to the appropriate page
 *
 */
@ManagedBean
@Named
@SessionScoped
public class BlogController {
    
    @Inject
    BlogBusinessInterface service;
    
    // Used to push to DB
    public String onSubmitCreate(Blog blog) 
    {
        System.out.println("We Will use this to push to DB\n\n");

        //
        // Add blog to list and write to file
        //
        
        User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        blog.setAuth_id(user.getUser_id());
        service.insertOne(blog);
                
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("blog", blog);
        
        return "listing_blog.xhtml";
    }
 
    public String onClickEdit(Blog blog) {

        System.out.println("BlogController onClickEdit() Executing.");
        System.out.println("Blog: " + blog.toString());

        // Put value in the session map so form will see it - request map does not work.
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("blog", blog);
 
        return "edit_blog.xhtml";
    }

    public String onSubmitEdit(Blog blog) {
    
        System.out.println("onSubmitEdit: Blog " + blog.toString());

        // Call service to submit blog update
        service.updateOne(blog);
        
        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
    
    public String onCancelEdit() {
    
        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
    
    public String onClickDelete(Blog blog) {

        System.out.println("BlogController onClickDelete() Executing.");
        System.out.println("Blog: " + blog.toString());

        // Put value in the session map so form will see it - request map does not work.
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("blog", blog);
        
        return "delete_blog.xhtml";
    }

    public String onSubmitDelete(Blog blog) {
    
        System.out.println("onSubmitDelete: Delete Confirmed - Blog " + blog.toString());

        // Call service to delete blog from database
        service.deleteOne(blog.getBlog_id());

        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
    
    public String onCancelDelete() {
    
        System.out.println("onCancelDelete: Delete Cancelled");

        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
       
    
    public BlogBusinessInterface getService() {
        
        return service;
    }  
 }
