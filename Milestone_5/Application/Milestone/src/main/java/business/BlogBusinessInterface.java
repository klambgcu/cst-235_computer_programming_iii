package business;

import beans.Blog;
import javax.ejb.Local;
import java.util.List;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Local
public interface BlogBusinessInterface {
   
    public List<Blog> getBlogs();   

    public int deleteOne(int id);

    public int insertOne(Blog blog);

    public List<Blog> readAll();

    public int updateOne(Blog blog);
    
    public Blog selectBlogById(int id);

    public List<Blog> searchForBlogsLikeTitle(String searchTitle);

}
