package business;

import beans.Blog;
import beans.BlogDTO;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@RequestScoped
@Path("/blogs")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class BlogRestService {

    @Inject
    BlogBusinessInterface service;

    @GET
    @Path("/getjson")
    @Produces(MediaType.APPLICATION_JSON)
    public BlogDTO getAllBlogsAsJson() {

        BlogDTO blogDTO = new BlogDTO();

        blogDTO.setBlogs(service.readAll());

        if (blogDTO.getBlogs().isEmpty()) {
            blogDTO.setCode(0);
            blogDTO.setMessage( "No Blogs Created.");
        } else {
            blogDTO.setCode(blogDTO.getBlogs().size());
            blogDTO.setMessage("All Blogs Returned.");
        }

        return blogDTO;
    }

    @GET
    @Path("/getjsonbyid/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public BlogDTO getBlogsById(@PathParam("id") int id) {

        BlogDTO blogDTO = new BlogDTO();
        Blog blog = service.selectBlogById(id);

        if (blog == null) {
            blogDTO.setCode(0);
            blogDTO.setMessage("Invalid ID Requested.");
        } else {
            blogDTO.getBlogs().add(blog);
            blogDTO.setCode(1);
            blogDTO.setMessage("Blog Requested Returned.");
        }

        return blogDTO;
    }

    @GET
    @Path("/getjsonbytitle/{searchtitle}")
    @Produces(MediaType.APPLICATION_JSON)
    public BlogDTO getBlogsById(@PathParam("searchtitle") String searchtitle) {

        BlogDTO blogDTO = new BlogDTO();
        blogDTO.setBlogs(service.searchForBlogsLikeTitle(searchtitle));

        if (blogDTO.getBlogs().isEmpty()) {
            blogDTO.setCode(0);
            blogDTO.setMessage("Invalid Title Requested");
        } else {
            blogDTO.setCode(blogDTO.getBlogs().size());
            blogDTO.setMessage("Returning all blogs found.");
        }

        return blogDTO;
    }
}
