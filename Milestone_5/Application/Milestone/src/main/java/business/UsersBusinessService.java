package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.List;
import beans.User;
import database.DatabaseService;
import javax.ejb.EJB;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */


@Stateless
@Local(UserBusinessInterface.class)
public class UsersBusinessService implements UserBusinessInterface {
    
    private final DatabaseService db = new DatabaseService();

    @Override
    public List<User> getUsers() {
        return db.readAllUsers();
    }

    @Override
    public Boolean userAlreadyExists(String usrName) {
        //return Users.getInstance().userAlreadyExists(usrName);
        User user = db.selectUserByUsrName(usrName);
        return (user != null);
    }
    
    @Override
    public Boolean isValidUser(User user) {
        //return Users.getInstance().isValidUser(user);
        User dbuser = db.selectUserByUsrName(user.getUsrName());
        return ( (dbuser != null &&
                  dbuser.getUsrName().equals(user.getUsrName()) &&
                  dbuser.getPassword().equals(user.getPassword())) );
    }
    
    @Override
    public User getUserByUsrName(String usrName) {
        return db.selectUserByUsrName(usrName);
    }
    
    @Override
    public User getUserById(int id) {
        return db.selectUserById(id);
    }
    
    @Override
    public int insertOne(User user) {
        return db.insertOne(user);
    }

}
