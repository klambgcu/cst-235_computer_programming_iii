package business;

import beans.User;
import javax.ejb.Local;
import java.util.List;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Local
public interface UserBusinessInterface {
   
    public List<User> getUsers();

    public Boolean userAlreadyExists(String usrName);
    
    public Boolean isValidUser(User user);
    
    public User getUserByUsrName(String usrName);
    
    public User getUserById(int id);
    
    public int insertOne(User user);

}
