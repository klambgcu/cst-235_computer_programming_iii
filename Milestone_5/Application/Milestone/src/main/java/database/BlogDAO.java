package database;

import beans.Blog;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * Concrete class that contains CRUD database methods for blogs table/objects
 * @author Kelly Lamb
 */
public class BlogDAO {

    private final static String CONNECT_CLASS = "com.mysql.cj.jdbc.Driver";
    private final static String DBURL = "jdbc:mysql://localhost:3306/cst_235";
    private final static String DBBLOG = "group2";
    private final static String DBPASS = "group2";

    private final static String SQL_SELECT_ALL_BLOG = "select * from blog";
    private final static String SQL_INSERT_BLOG = "insert into blog (title, auth_id, date, blog, is_edited, edited_date) values (?, ?, ?, ?, ?, ?)";
    private final static String SQL_SELECT_BLOG_BY_ID = "select * from blog where blog_id = ?";
    private final static String SQL_SELECT_BLOG_LIKE_TITLE = "select * from blog where title like ?";
    private final static String SQL_DELETE_BLOG_BY_ID = "delete from blog where blog_id = ?";
    private final static String SQL_UPDATE_BLOG = "update blog set title = ?, blog = ?, is_edited = ?, edited_date = ? where blog_id = ?";

    /**
     * 
     * Default Constructor - Contains CRUD database methods for blogs table/objects
     */
    
    public BlogDAO() {
    }

    /**
     * 
     * @return List of all blogs in the database
     */
    public List<Blog> readAllBlogs() {
        List<Blog> blogs = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_BLOG);) {

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int blog_id = rs.getInt("blog_id");
                String title = rs.getString("title");
                int auth_id = rs.getInt("auth_id");
                java.util.Date date = rs.getDate("date");
                String blog = rs.getString("blog");
                Boolean is_edited = rs.getBoolean("is_edited");
                java.util.Date  edited_date = rs.getDate("edited_date");
                blogs.add(new Blog(blog_id, title, auth_id, date, blog, is_edited, edited_date));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return blogs;
    }

    /**
     * 
     * @param blog : Blog object to insert into the database
     * @return : Integer  - number of rows affected
     */
    public int insertOne(Blog blog) {
        int rowsAffected = 0;

        // private final static String SQL_INSERT_BLOG = "insert into blog (title, auth_id, date, blog, is_edited, edited_date) values (?, ?, ?, ?, ?, ?)";

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_BLOG, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, blog.getTitle());
            preparedStatement.setInt(2, blog.getAuth_id());
            preparedStatement.setDate(3, new java.sql.Date(blog.getDate().getTime()));
            preparedStatement.setString(4, blog.getBlog());
            preparedStatement.setBoolean(5, blog.getIsEdited());
            preparedStatement.setDate(6, new java.sql.Date(blog.getEdited_date().getTime()));

            rowsAffected = preparedStatement.executeUpdate();

            if (rowsAffected == 0) {
                throw new SQLException("Creating blog failed, no rows affected.");
            }
            
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    blog.setBlog_id(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException("Creating blog failed, no ID obtained.");
                }
            } catch (SQLException e) {
                printSQLException(e);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return rowsAffected;
    }

    /**
     * 
     * @param id : Integer reference blog_id to delete
     * @return : Integer - number of rows affected
     */
    public Blog selectBlogById(int id) {
        Blog blog = null;

        // blog_id, title, auth_id, date, blog, is_edited, edited_date
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BLOG_BY_ID);) {

            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int blog_id = rs.getInt("blog_id");
                String title = rs.getString("title");
                int auth_id = rs.getInt("auth_id");
                java.util.Date date = rs.getDate("date");
                String blogEntry = rs.getString("blog");
                Boolean is_edited = rs.getBoolean("is_edited");
                java.util.Date edited_date = rs.getDate("edited_date");
                blog = new Blog(blog_id, title, auth_id, date, blogEntry, is_edited, edited_date);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return blog;
    }
    
        /**
     * 
     * @return List of all blogs in the database
     */
    public List<Blog> searchForBlogsLikeTitle(String searchTitle) {
        List<Blog> blogs = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BLOG_LIKE_TITLE);) {

            preparedStatement.setString(1, "%" + searchTitle + "%");

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int blog_id = rs.getInt("blog_id");
                String title = rs.getString("title");
                int auth_id = rs.getInt("auth_id");
                java.util.Date date = rs.getDate("date");
                String blog = rs.getString("blog");
                Boolean is_edited = rs.getBoolean("is_edited");
                java.util.Date  edited_date = rs.getDate("edited_date");
                blogs.add(new Blog(blog_id, title, auth_id, date, blog, is_edited, edited_date));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return blogs;
    }

    
    /**
     * 
     * @param id : Integer reference blog_id to delete
     * @return : Integer - number of rows affected
     */
    public int deleteBlogById(int id) {
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BLOG_BY_ID);) {

            preparedStatement.setInt(1, id);

            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
        return rowsAffected;
    }

    /**
     * 
     * @param blog : Blog - object to update
     * @return : Integer - number of rows affected.
     */
    public int updateBlog(Blog blog) {
        int rowsAffected = 0;
        
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_BLOG);) {
            
            preparedStatement.setString(1, blog.getTitle());
            preparedStatement.setString(2, blog.getBlog());
            preparedStatement.setBoolean(3, blog.getIsEdited());
            preparedStatement.setDate(4, new java.sql.Date(blog.getEdited_date().getTime()));
            preparedStatement.setInt(5, blog.getBlog_id());

            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }

        return rowsAffected;
    }
    
    // --------------------------------------------------------
    // Helper methods below
    // --------------------------------------------------------
    
    /**
     *
     * @return connection : a helper method to return a connection to the database
     */
    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(CONNECT_CLASS);
            connection = DriverManager.getConnection(DBURL, DBBLOG, DBPASS);
        } catch (SQLException e) {
            printSQLException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * 
     * @param exceptions : a helper method to log exception error notifications
     */
    protected void printSQLException(SQLException exceptions) {
        for (Throwable e : exceptions) {
            if (e instanceof SQLException) {
                e.printStackTrace();
                System.out.println("SQLState: " + ((SQLException) e).getSQLState());
                System.out.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.out.println("Message: " + e.getMessage());
                Throwable t = exceptions.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}
