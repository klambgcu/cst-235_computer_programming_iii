package database;

import beans.Blog;
import beans.User;
import java.util.List;
import javax.ejb.Local;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * 
 * Definition for the Database Interface
 */
@Local
public interface DatabaseInterface {

    //
    // All exposed methods for the User DAO
    //
    public List<User> readAllUsers();
    public int insertOne(User user);
    public User selectUserById(int id);
    public User selectUserByUsrName(String usrName);
    public int deleteUserById(int id);
    public int updateUser(User user);

    //
    // All exposed methods for the Blog DAO
    //
    public List<Blog> readAllBlogs();
    public int insertOne(Blog blog);
    public Blog selectBlogById(int id);
    public int deleteBlogById(int id);
    public int updateBlog(Blog blog);
    public List<Blog> searchForBlogsLikeTitle(String searchTitle);
}
