package database;

import beans.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * Concrete class that contains CRUD database methods for users table/objects
 * @author Kelly Lamb
 */
public class UserDAO {

    private final static String CONNECT_CLASS = "com.mysql.cj.jdbc.Driver";
    private final static String DBURL = "jdbc:mysql://localhost:3306/cst_235";
    private final static String DBUSER = "group2";
    private final static String DBPASS = "group2";

    private final static String SQL_SELECT_ALL_USER = "select * from user";
    private final static String SQL_INSERT_USER = "insert into user (firstname, lastname, usrname, password, experience) values (?, ?, ?, ?, ?)";
    private final static String SQL_SELECT_USER_BY_ID = "select * from user where user_id = ?";
    private final static String SQL_DELETE_USER_BY_ID = "delete from user where user_id = ?";
    private final static String SQL_UPDATE_USER = "update user set firstname =?, lastname = ?, usrname = ?, password = ?, experience = ? where user_id = ?";
    private final static String SQL_SELECT_USER_BY_USRNAME = "select * from user where usrname = ?";

    /**
     * 
     * Default Constructor - Contains CRUD database methods for users table/objects
     */
    
    public UserDAO() {
    }

    /**
     * 
     * @return List of all users in the database
     */
    public List<User> readAllUsers() {
        List<User> users = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_USER);) {

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int user_id = rs.getInt("user_id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String usrname = rs.getString("usrname");
                String password = rs.getString("password");
                String experience = rs.getString("experience");
                users.add(new User(user_id, firstname, lastname, usrname, password, experience));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return users;
    }

    /**
     * 
     * @param user : User object to insert into the database
     * @return : Integer  - number of rows affected
     */
    public int insertOne(User user) {
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getUsrName());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getExperience());

            rowsAffected = preparedStatement.executeUpdate();
            
            if (rowsAffected == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setUser_id(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            } catch (SQLException e) {
                printSQLException(e);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return rowsAffected;
    }

    /**
     * 
     * @param id : Integer reference user_id to delete
     * @return : Integer - number of rows affected
     */
    public User selectUserById(int id) {
        User user = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID);) {

            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int user_id = rs.getInt("user_id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String usrname = rs.getString("usrname");
                String password = rs.getString("password");
                String experience = rs.getString("experience");
                user = new User(user_id, firstname, lastname, usrname, password, experience);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return user;
    }
    
    /**
     * 
     * @param id : Integer reference user_id to delete
     * @return : Integer - number of rows affected
     */
    public User selectUserByUsrName(String usrName) {
        User user = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_USRNAME);) {

            preparedStatement.setString(1, usrName);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int user_id = rs.getInt("user_id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String usrname = rs.getString("usrname");
                String password = rs.getString("password");
                String experience = rs.getString("experience");
                user = new User(user_id, firstname, lastname, usrname, password, experience);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return user;
    }

    /**
     * 
     * @param id : Integer reference user_id to delete
     * @return : Integer - number of rows affected
     */
    public int deleteUserById(int id) {
        int rowsAffected = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID);) {

            preparedStatement.setInt(1, id);

            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
        return rowsAffected;
    }

    /**
     * 
     * @param user : User - object to update
     * @return : Integer - number of rows affected.
     */
    public int updateUser(User user) {
        int rowsAffected = 0;
        
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);) {
            
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getUsrName());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getExperience());
            preparedStatement.setInt(6, user.getUser_id());

            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }

        return rowsAffected;
    }
    
    // --------------------------------------------------------
    // Helper methods below
    // --------------------------------------------------------
    
    /**
     *
     * @return connection : a helper method to return a connection to the database
     */
    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(CONNECT_CLASS);
            connection = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
        } catch (SQLException e) {
            printSQLException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * 
     * @param exceptions : a helper method to log exception error notifications
     */
    protected void printSQLException(SQLException exceptions) {
        for (Throwable e : exceptions) {
            if (e instanceof SQLException) {
                e.printStackTrace();
                System.out.println("SQLState: " + ((SQLException) e).getSQLState());
                System.out.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.out.println("Message: " + e.getMessage());
                Throwable t = exceptions.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}
