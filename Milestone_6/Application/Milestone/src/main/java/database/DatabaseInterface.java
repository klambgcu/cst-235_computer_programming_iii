package database;

import beans.Blog;
import beans.User;
import java.util.List;
import javax.ejb.Local;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * 
 * Definition for the Database Interface
 */
@Local
public interface DatabaseInterface {

    //
    // All exposed methods for the User DAO
    //
    
    /**
     * 
     * @return List : All users from the database
     */
    public List<User> readAllUsers();
    
    /**
     * 
     * @param user : User to insert into the database
     * @return Integer : Number of rows affected 
     */
    public int insertOne(User user);
    
    /**
     * 
     * @param id: Integer ID (Key) to select user
     * @return User: User object if found otherwise null
     */
    public User selectUserById(int id);
    
    /**
     * 
     * @param usrName : String name to search for user
     * @return User: User object if found otherwise null
     */
    public User selectUserByUsrName(String usrName);
    
    /**
     * 
     * @param id : Integer ID (Key) of user to delete from database
     * @return Integer : Number of rows affected 
     */
    public int deleteUserById(int id);
    
    /**
     * 
     * @param user : User object to update
     * @return Integer: Number of rows affected
     */
    public int updateUser(User user);

    //
    // All exposed methods for the Blog DAO
    //
    
    /**
     * 
     * @return List: List of all blogs in the database
     */
    public List<Blog> readAllBlogs();
    
    /**
     * 
     * @param blog : Blog object to insert into the database
     * @return Integer: Number of rows affected
     */
    public int insertOne(Blog blog);
    
    /**
     * 
     * @param id : Integer ID (Key) to select blog
     * @return Blog : Object if found otherwise null
     */
    public Blog selectBlogById(int id);
    
    /**
     * 
     * @param id: Integer ID (Key) to blog to delete from database
     * @return Integer : Number of rows affected
     */
    public int deleteBlogById(int id);
    
    /**
     * 
     * @param blog: Blog object to update. Uses ID to find item
     * @return Integer: Number of rows affected
     */
    public int updateBlog(Blog blog);
    
    /**
     * 
     * @param searchTitle : String to find blog where searchTitle is in title
     * @return List : List of Blogs found containing searchTitle in Blog Title
     */
    public List<Blog> searchForBlogsLikeTitle(String searchTitle);
}
