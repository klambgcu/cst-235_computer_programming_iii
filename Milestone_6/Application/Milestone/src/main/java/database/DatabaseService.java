package database;

import beans.Blog;
import beans.User;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 *
 * Implementation for the Database Interface
 * Heavily relies on UserDAO and BlogDAO implementations
 */
@Stateless
@Local(DatabaseInterface.class)
public class DatabaseService implements DatabaseInterface{
    
    private UserDAO userDAO = new UserDAO();
    private BlogDAO blogDAO = new BlogDAO();
    
    /**
     * 
     * Default Constructor
     */
    public DatabaseService() {
        
    }
    
    /**
     * 
     * @return List : All users from the database
     */
    public List<User> readAllUsers() {
        return userDAO.readAllUsers();
    }

    /**
     * 
     * @param user : User to insert into the database
     * @return Integer : Number of rows affected 
     */    
    public int insertOne(User user) {
        return userDAO.insertOne(user);
    }
    
    /**
     * 
     * @param id: Integer ID (Key) to select user
     * @return User: User object if found otherwise null
     */
    public User selectUserById(int id) {
        return userDAO.selectUserById(id);
    }
    
    /**
     * 
     * @param usrName : String name to search for user
     * @return User: User object if found otherwise null
     */
    public User selectUserByUsrName(String usrName) {
        return userDAO.selectUserByUsrName(usrName);
    }
    
     /**
     * 
     * @param id : Integer ID (Key) of user to delete from database
     * @return Integer : Number of rows affected 
     */
    public int deleteUserById(int id) {
        return userDAO.deleteUserById(id);
    }
    
    /**
     * 
     * @param user : User object to update
     * @return Integer: Number of rows affected
     */
    public int updateUser(User user) {
        return userDAO.updateUser(user);
    }
    

    //
    // All exposed methods for the Blog DAO
    //
    
    /**
     * 
     * @return List : List of all blogs in the database
     */
    public List<Blog> readAllBlogs() {
        return blogDAO.readAllBlogs();
    }

    /**
     * 
     * @param blog : Blog object to insert into the database
     * @return Integer: Number of rows affected
     */
    public int insertOne(Blog blog) {
        return blogDAO.insertOne(blog);
    }
    
    /**
     * 
     * @param id : Integer ID (Key) to select blog
     * @return Blog : Object if found otherwise null
     */
    public Blog selectBlogById(int id) {
        return blogDAO.selectBlogById(id);
    }
    
    /**
     * 
     * @param id: Integer ID (Key) to blog to delete from database
     * @return Integer : Number of rows affected
     */
    public int deleteBlogById(int id) {
        return blogDAO.deleteBlogById(id);
    }
    
    /**
     * 
     * @param blog: Blog object to update. Uses ID to find item
     * @return Integer: Number of rows affected
     */
    public int updateBlog(Blog blog) {
        return blogDAO.updateBlog(blog);
    }
    
    /**
     * 
     * @param searchTitle : String to find blog where searchTitle is in title
     * @return List : List of Blogs found containing searchTitle in Blog Title
     */
    public List<Blog> searchForBlogsLikeTitle(String searchTitle) {
        return blogDAO.searchForBlogsLikeTitle(searchTitle);
    }
    
}
