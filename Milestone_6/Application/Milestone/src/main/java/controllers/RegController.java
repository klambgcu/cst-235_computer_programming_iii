package controllers;

import beans.User;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import business.UserBusinessInterface;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Controller to handle registration process for bloggers
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle User Registration
 *
 */
@ManagedBean
public class RegController {

    @EJB
    UserBusinessInterface service;
    
    
    /**
     * Handles User Registration.
     * Determines if user account already exists (fails) or
     * Allows the user account creation
     * @return String: Next form for application (index or registration)
     */
    public String onSubmit() {

        FacesContext context = FacesContext.getCurrentInstance();
        User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

        //
        // Check if user already registered
        //
        if (service.userAlreadyExists(user.getUsrName()) == false) {
            //
            // Add user to database
            //
            service.insertOne(user);
            user = service.getUserByUsrName(user.getUsrName()); // Load user id after insert
            
            System.out.println("RegController: User = " + user.toString());
            

            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
            return "index.xhtml?faces-redirect=true"; // redirect to protect area form, if necessary
        } else {
            
            // Add View Faces Message
            String errorMessage = "User Account Already Exists.";
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, errorMessage);

            // Add the message into context for a specific component
            FacesContext.getCurrentInstance().addMessage("reg-form:message", message);
            
            return "registration.xhtml";
        }

    }
}
