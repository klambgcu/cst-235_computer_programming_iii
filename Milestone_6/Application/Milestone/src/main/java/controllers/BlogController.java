package controllers;

import beans.Blog;
import beans.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import business.BlogBusinessInterface;
import java.util.List;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Controller to handle blog CRUD.
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * Controller to handle Blogging actions to the appropriate page
 *
 */
@ManagedBean
@Named
@SessionScoped
public class BlogController {
    
    @Inject
    BlogBusinessInterface service;
    
    // Static List required for the PrimeFacecs 
    // dataTable sorting/pagination Options used
    // for Report 1.
    public static List<Blog> blogReportList;
    
    /**
     * 
     * @param blog : Blog to create and push to the database
     * @return String : Next form
     */
    public String onSubmitCreate(Blog blog) 
    {
        System.out.println("We Will use this to push to DB\n\n");

        //
        // Add blog to list and write to file
        //
        
        User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        blog.setAuth_id(user.getUser_id());
        service.insertOne(blog);
                
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("blog", blog);
        
        return "listing_blog.xhtml";
    }
 
    /**
     * 
     * @param blog : Blog to edit
     * @return String : Next form
     */
    public String onClickEdit(Blog blog) {

        System.out.println("BlogController onClickEdit() Executing.");
        System.out.println("Blog: " + blog.toString());

        // Put value in the session map so form will see it - request map does not work.
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("blog", blog);
 
        return "edit_blog.xhtml";
    }

    /**
     * 
     * @param blog : Blog to edit and submit to database
     * @return String : name of next form
     */
    public String onSubmitEdit(Blog blog) {
    
        System.out.println("onSubmitEdit: Blog " + blog.toString());

        // Call service to submit blog update
        service.updateOne(blog);
        
        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
    
    /**
     * 
     * @return String : Next form after cancelling edit
     */
    public String onCancelEdit() {
    
        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
    
    /**
     * 
     * @param blog : Blog selected to show for deletion
     * @return String : deletion confirmation page
     */
    public String onClickDelete(Blog blog) {

        System.out.println("BlogController onClickDelete() Executing.");
        System.out.println("Blog: " + blog.toString());

        // Put value in the session map so form will see it - request map does not work.
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("blog", blog);
        
        return "delete_blog.xhtml";
    }

    /**
     * 
     * @param blog : Blog to delete after confirmation and removal from database
     * @return String : next form to display
     */
    public String onSubmitDelete(Blog blog) {
    
        System.out.println("onSubmitDelete: Delete Confirmed - Blog " + blog.toString());

        // Call service to delete blog from database
        service.deleteOne(blog.getBlog_id());

        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
    
    /**
     * 
     * @return String : next form to display after cancelling in delete confirmation
     */
    public String onCancelDelete() {
    
        System.out.println("onCancelDelete: Delete Cancelled");

        // Remove the blog from the session map and return to listing page
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("blog");

        return "listing_blog.xhtml";
    }
       
    /**
     * 
     * @param blog : Blog for title to use for searching/filtering report (Blank means all records)
     * @return String : next form to display - report listing
     */
    public String onSubmitReport1(Blog blog) {
        
        if (blog.getTitle() == null || blog.getTitle().trim().equals(""))
        {
            blogReportList = service.getBlogs();
        } else {
            blogReportList = service.searchForBlogsLikeTitle(blog.getTitle());
        }

        return "report_blog.xhtml";
    }
    
    /**
     * 
     * @return List : List of blogs found for report title search criteria
     */
    public List<Blog> getBlogReportList() {
                
        return this.blogReportList;
    }
    
    /**
     * 
     * @return BlogBusinessInterface to access business service object
     */
    public BlogBusinessInterface getService() {
        
        return service;
    }  
 }
