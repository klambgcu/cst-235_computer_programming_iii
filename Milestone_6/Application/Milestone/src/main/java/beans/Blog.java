package beans;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.faces.bean.ManagedBean;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Singleton to obtain list of users from file
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@ManagedBean
public final class Blog {

    private int blog_id;
    private String title;
    private int auth_id;
    private Date date;
    private String blog;
    private Boolean isEdited;
    private Date edited_date;

    /**
     * 
     * Default Constructor
     */
    public Blog() {
        Date currentDate = getDateWithoutTime();
        this.blog_id = 0;
        this.title = "";
        this.auth_id = 0;
        this.date = currentDate;
        this.blog = "";
        this.isEdited = false;
        this.edited_date = currentDate;
    }

    /**
     * Constructor - full parameters
     * @param blog_id : Integer for identifying the blog (Key)
     * @param title : String for the title of the blog
     * @param auth_id : Integer for the Author ID (Key)
     * @param date : Date for creation of the blog
     * @param blog : String contents of the blog entry
     * @param isEdited : Boolean true if edited, false is not
     * @param edited_date : Date of last edit
     */
    public Blog(int blog_id, String title, int auth_id, Date date, String blog, Boolean isEdited, Date edited_date) {
        this.blog_id = blog_id;
        this.title = title;
        this.auth_id = auth_id;
        this.date = date;
        this.blog = blog;
        this.isEdited = isEdited;
        this.edited_date = edited_date;
    }
    
    /**
     * *
     *
     * @param title - Title : S
     * @param user_id - Author's ID : S
     * @param blog - Blog Text : S
     * @param isEdited - If edited : B
     */
    public Blog(String title, int user_id, String blog, boolean isEdited) {
        this.blog_id = hashCode(); // Override this with id from database
        this.title = title;
        this.auth_id = user_id;
        this.date = getDateWithoutTime();
        this.blog = blog;
        this.isEdited = isEdited;
        this.edited_date = getDateWithoutTime();
    }

    private Date getDateWithoutTime()
    {
        Date dateWithoutTime = new Date();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateWithoutTime = sdf.parse(sdf.format(new Date()));
        } catch (Exception e) {e.printStackTrace();}// Do nothing
        
        return dateWithoutTime;
    }
    
    /**
     * 
     * @param b : Blog object to edit
     */
    public void edit_blog(Blog b) {
        this.blog = b.getBlog();
        this.title = b.getTitle();
        this.isEdited = true;
        this.edited_date = getDateWithoutTime();
    }

    /**
     * 
     * @return String: Fields delimited by tab - useful for text file database
     */
    public String toStringTabDelimited() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
        return this.title + "\t" + 
               this.auth_id + "\t" +
               this.blog + "\t" +
               this.isEdited + "\t" +
               formatter.format(this.date) + "\t" +
               formatter.format(this.edited_date) + "\t" +
               System.lineSeparator();
    }

    /**
     * 
     * @return String representation of Blog object fields
     */
    @Override
    public String toString() {
        return "Blog{" + 
                "blog_id=" + blog_id + 
                ", title=" + title +
                ", auth_id=" + auth_id + 
                ", date=" + date + 
                ", blog=" + blog + 
                ", isEdited=" + isEdited + 
                ", edited_date=" + edited_date + '}';
    }

    /**
     * 
     * @return Integer : hash value for object
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.blog_id);
        hash = 97 * hash + Objects.hashCode(this.title);
        hash = 97 * hash + Objects.hashCode(this.auth_id);
        hash = 97 * hash + Objects.hashCode(this.date);
        hash = 97 * hash + Objects.hashCode(this.blog);
        hash = 97 * hash + Objects.hashCode(this.isEdited);
        return hash;
    }

    /**
     * 
     * @param obj : Object use to compare for equality with this object
     * @return Boolean : true if equal, false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        boolean isEqual = true;
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            isEqual = false;
        } else if (getClass() != obj.getClass()) {
            isEqual = false;
        }
        final Blog other = (Blog) obj;
        if (!Objects.equals(this.blog_id, other.blog_id)) {
            isEqual = false;
        }
        if (!Objects.equals(this.title, other.title)) {
            isEqual = false;
        }
        if (!Objects.equals(this.auth_id, other.auth_id)) {
            isEqual = false;
        }
        if (!Objects.equals(this.blog, other.blog)) {
            isEqual = false;
        }
        return isEqual;
    }

    /**
     * 
     * @return Integer Blog ID
     */
    public int getBlog_id() {
        return blog_id;
    }

    /**
     * 
     * @param blog_id : Integer to set the blog ID
     */
    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    /**
     * 
     * @return String for the Blog Title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title : String to set the Blog Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return Integer : Author Id
     */
    public int getAuth_id() {
        return auth_id;
    }

    /**
     * 
     * @param auth_id : Integer to set Author Id
     */
    public void setAuth_id(int auth_id) {
        this.auth_id = auth_id;
    }

    /**
     * 
     * @return Date: Date Blog created
     */
    public Date getDate() {
        return date;
    }

    /**
     * 
     * @param date : Date of the Blog creation
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * 
     * @return String containing the blog content
     */
    public String getBlog() {
        return blog;
    }

    /**
     * 
     * @param blog : String to set the blog content
     */
    public void setBlog(String blog) {
        this.blog = blog;
    }

    /**
     * 
     * @return Boolean : true if blog was edited
     */
    public Boolean getIsEdited() {
        return isEdited;
    }

    /**
     * 
     * @param isEdited : Boolean to indicate if Blog is edited
     */
    public void setIsEdited(Boolean isEdited) {
        this.isEdited = isEdited;
    }

    /**
     * 
     * @return Date when blog was edited, different from Date of creation
     * if edited is true (usually)
     */
    public Date getEdited_date() {
        return edited_date;
    }

    /**
     * 
     * @param date : Date to set when Blog content was edited
     */
    public void setEdited_date(Date date) {
        this.edited_date = date;
    }

    
}
