@echo off
call mvn clean package
call docker build -t group2/Milestone .
call docker rm -f Milestone
call docker run -d -p 9080:9080 -p 9443:9443 --name Milestone group2/Milestone