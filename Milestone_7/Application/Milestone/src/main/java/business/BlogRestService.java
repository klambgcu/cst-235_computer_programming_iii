package business;

import beans.Blog;
import beans.BlogDTO;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Handle RESTful operations (JSON)
 * 2. /rest/blogs/getjson
 * 3. /rest/blogs/getjsonbyid/{id}
 * 4. /rest/blogs//getjsonbytitle/{searchtitle}
 * ---------------------------------------------------------------
 */

@RequestScoped
@Path("/blogs")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class BlogRestService {

    @Inject
    BlogBusinessInterface service;

    /**
     * 
     * @return BlogDTO: JSON content
     * A List of all blogs in the database
     */
    @GET
    @Path("/getjson")
    @Produces(MediaType.APPLICATION_JSON)
    public BlogDTO getAllBlogsAsJson() {

        BlogDTO blogDTO = new BlogDTO();

        blogDTO.setBlogs(service.readAll());

        if (blogDTO.getBlogs().isEmpty()) {
            blogDTO.setCode(0);
            blogDTO.setMessage( "No Blogs Created.");
        } else {
            blogDTO.setCode(blogDTO.getBlogs().size());
            blogDTO.setMessage("All Blogs Returned.");
        }

        return blogDTO;
    }

    /**
     * 
     * @param id: Integer to find specific blog by ID (Key)
     * @return BlogDTO: Contains blog encapsulated in BlogDTO if found,
     * otherwise a message and status reflect results
     */
    @GET
    @Path("/getjsonbyid/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public BlogDTO getBlogsById(@PathParam("id") int id) {

        BlogDTO blogDTO = new BlogDTO();
        Blog blog = service.selectBlogById(id);

        if (blog == null) {
            blogDTO.setCode(0);
            blogDTO.setMessage("Invalid ID Requested.");
        } else {
            blogDTO.getBlogs().add(blog);
            blogDTO.setCode(1);
            blogDTO.setMessage("Blog Requested Returned.");
        }

        return blogDTO;
    }

    /**
     * 
     * @param searchtitle: String to search for blogs where title contains searchtitle
     * @return BlogDTO: A list of all blogs matching criteria encapsulated in BlogDTO if found,
     * otherwise a message and status reflect results
     */
    @GET
    @Path("/getjsonbytitle/{searchtitle}")
    @Produces(MediaType.APPLICATION_JSON)
    public BlogDTO getBlogsById(@PathParam("searchtitle") String searchtitle) {

        BlogDTO blogDTO = new BlogDTO();
        blogDTO.setBlogs(service.searchForBlogsLikeTitle(searchtitle));

        if (blogDTO.getBlogs().isEmpty()) {
            blogDTO.setCode(0);
            blogDTO.setMessage("Invalid Title Requested");
        } else {
            blogDTO.setCode(blogDTO.getBlogs().size());
            blogDTO.setMessage("Returning all blogs found.");
        }

        return blogDTO;
    }
}
