package business;

import beans.User;
import javax.ejb.Local;
import java.util.List;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Handle User Business Logic
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Local
public interface UserBusinessInterface {
   
    /**
     * 
     * @return List : List of all users in the database
     */
    public List<User> getUsers();

    /**
     * 
     * @param usrName: String of user to check if already in database
     * @return Boolean: True if exists, false otherwise
     */
    public Boolean userAlreadyExists(String usrName);
    
    /**
     * 
     * @param user: User object to validate user
     * @return True if valid user, false otherwise
     */
    public Boolean isValidUser(User user);
    
    /**
     * 
     * @param usrName: String of user name to search database
     * @return User object if found, otherwise null
     */
    public User getUserByUsrName(String usrName);
    
    /**
     * 
     * @param id: Integer (Key) ID to find user in database
     * @return User object if found, otherwise null
     */
    public User getUserById(int id);
    
    /**
     * 
     * @param user: User object to insert into the database
     * @return Integer: number of rows affected
     */
    public int insertOne(User user);

}
