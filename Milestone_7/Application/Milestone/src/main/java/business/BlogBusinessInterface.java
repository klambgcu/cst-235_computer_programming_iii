package business;

import beans.Blog;
import javax.ejb.Local;
import java.util.List;


/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Handle Blog Business Logic 
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

@Local
public interface BlogBusinessInterface {
   
    /**
     * 
     * @return List : All blogs in the database
     */
    public List<Blog> getBlogs();   

    /**
     * 
     * @param id : Integer (KEY) to delete blog from database
     * @return Integer: number of rows affected
     */
    public int deleteOne(int id);

    /**
     * 
     * @param blog : Blog to insert into the database
     * @return Integer: number of rows affected
     */
    public int insertOne(Blog blog);

    /**
     * 
     * @return List : All blogs in the database
     */
    public List<Blog> readAll();

    /**
     * 
     * @param blog: Blog to update by ID (Key)
     * @return Integer: number of rows affected
     */
    public int updateOne(Blog blog);
    
    /**
     * 
     * @param id: Integer ID (Key) to select from database
     * @return Blog object
     */
    public Blog selectBlogById(int id);

    /**
     * 
     * @param searchTitle : String to find list of blogs containing title
     * @return List: List of blogs where title contained searchTitle
     */
    public List<Blog> searchForBlogsLikeTitle(String searchTitle);

}
