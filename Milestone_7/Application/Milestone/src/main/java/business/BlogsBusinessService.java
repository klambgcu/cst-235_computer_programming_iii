package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import java.util.List;
import beans.Blog;
import database.DatabaseService;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Handle Blog Business Logic Implementation
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */


@Stateless
@Local(BlogBusinessInterface.class)
@Alternative
public class BlogsBusinessService implements BlogBusinessInterface {

    private final DatabaseService db = new DatabaseService();

    /**
     * 
     * @return List : All blogs in the database
     */
    @Override
    public List<Blog> getBlogs()
    {
        return readAll();
    }

    /**
     * 
     * @param id : Integer (KEY) to delete blog from database
     * @return Integer: number of rows affected
     */
    @Override
    public int deleteOne(int id) {
        
        return db.deleteBlogById(id);
    }

    /**
     * 
     * @param blog : Blog to insert into the database
     * @return Integer: number of rows affected
     */
    @Override
    public int insertOne(Blog blog) {
        
        return db.insertOne(blog);
    }

    /**
     * 
     * @return List : All blogs in the database
     */
    @Override
    public List<Blog> readAll() {
        
        return db.readAllBlogs();
    }

    /**
     * 
     * @param blog: Blog to update by ID (Key)
     * @return Integer: number of rows affected
     */
    @Override
    public int updateOne(Blog blog) {
        blog.edit_blog(blog);
        return db.updateBlog(blog);
    }

    /**
     * 
     * @param id: Integer ID (Key) to select from database
     * @return Blog object
     */
    @Override
    public Blog selectBlogById(int id) {
        
        return db.selectBlogById(id);
    }

    /**
     * 
     * @param searchTitle : String to find list of blogs containing title
     * @return List: List of blogs where title contained searchTitle
     */
    @Override
    public List<Blog> searchForBlogsLikeTitle(String searchTitle) {
        
        return db.searchForBlogsLikeTitle(searchTitle);
    }

}
