package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.List;
import beans.User;
import database.DatabaseService;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-07
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Handle User Business Logic Implementation
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */
@Stateless
@Local(UserBusinessInterface.class)
public class UsersBusinessService implements UserBusinessInterface {
    
    private final DatabaseService db = new DatabaseService();

    /**
     * 
     * @return List : List of all users in the database
     */
    @Override
    public List<User> getUsers() {
        return db.readAllUsers();
    }

    /**
     * 
     * @param usrName: String of user to check if already in database
     * @return Boolean: True if exists, false otherwise
     */
    @Override
    public Boolean userAlreadyExists(String usrName) {
        //return Users.getInstance().userAlreadyExists(usrName);
        User user = db.selectUserByUsrName(usrName);
        return (user != null);
    }
    
    /**
     * 
     * @param user: User object to validate user
     * @return True if valid user, false otherwise
     */
    @Override
    public Boolean isValidUser(User user) {
        //return Users.getInstance().isValidUser(user);
        User dbuser = db.selectUserByUsrName(user.getUsrName());
        return ( (dbuser != null &&
                  dbuser.getUsrName().equals(user.getUsrName()) &&
                  dbuser.getPassword().equals(user.getPassword())) );
    }
    
    /**
     * 
     * @param usrName: String of user name to search database
     * @return User object if found, otherwise null
     */
    @Override
    public User getUserByUsrName(String usrName) {
        return db.selectUserByUsrName(usrName);
    }
    
    /**
     * 
     * @param id: Integer (Key) ID to find user in database
     * @return User object if found, otherwise null
     */
    @Override
    public User getUserById(int id) {
        return db.selectUserById(id);
    }
    
    /**
     * 
     * @param user: User object to insert into the database
     * @return Integer: number of rows affected
     */
    @Override
    public int insertOne(User user) {
        return db.insertOne(user);
    }

}
