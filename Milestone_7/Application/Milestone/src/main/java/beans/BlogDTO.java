package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Group 2 (Angel M., Kelly L., Mark P.)
 * Date      : 2021-03-14
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - CLC Group Assignment
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Singleton to obtain list of users from file
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

/**
 * 
 * Data Transfer Object DTO for Blog Requests
 * Contains list of blogs, code as integer, message as string
 */
public class BlogDTO implements Serializable {

	private List<Blog> blogs = null;
	private int code = 0;
	private String message = "";
	
	public BlogDTO()
	{
		blogs = new ArrayList<>();
		code = 0;
		message = "";
	}
	
	public BlogDTO(List<Blog> blogs, int code, String message)
	{
		this.blogs = blogs;
		this.code = code;
		this.message = message;
	}
	
	public List<Blog> getBlogs() {
	
		return this.blogs;
	}
	
	public void setBlogs(List<Blog> blogs) {
	
		this.blogs = blogs;
	}

	public int getCode() {
	
		return this.code;
	}

	public void setCode(int code) {
	
		this.code = code;
	}

	public String getMessage() {
	
		return this.message;
	}

	public void setMessage(String message) {
	
		this.message = message;
	}
}